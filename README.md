# Dotfiles for Linux Install
## Requirements
1. Fresh Linux Install, preferably Arch Linux
  * Systemd
  * ZSH
  * Oh-My-Zsh `https://github.com/robbyrussell/oh-my-zsh`
2. Vim +7
  * YouCompleteMe
