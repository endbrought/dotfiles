#!/usr/bin/sh
set -e
cp ../zshrc ~/.zshrc
cp -r ../alias ../cosmetic ../env ../utils ~/.zsh
